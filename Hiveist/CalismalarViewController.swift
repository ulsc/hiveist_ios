//
//  CalismalarViewController.swift
//  Hiveist
//
//  Created by Ulas Can Cengiz on 30/09/15.
//  Copyright © 2015 iOSBootcamp. All rights reserved.
//

import UIKit
import Parse
import CoreLocation

class CalismalarViewController: UIViewController, CLLocationManagerDelegate, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet var tableView: UITableView!
    
    var tableViewDataSource : [PFObject] = []
    
    var manager:CLLocationManager!
    
    var refreshControl:UIRefreshControl!
    
    @IBOutlet var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        manager = CLLocationManager()
        manager.delegate = self
        manager.requestWhenInUseAuthorization()
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.startUpdatingLocation()

        // Do any additional setup after loading the view.
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl.addTarget(self, action: "refresh", forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(refreshControl)
        
        self.searchBar.delegate = self
    }
    
    @IBAction func logoutTapped(sender: AnyObject)
    {
        PFUser.logOut()
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func viewWillAppear(animated: Bool)
    {
        refresh()
    }
    
    func refresh()
    {
        searchBar.text = ""
        self.view.endEditing(true)
        getCalismalar()
    }
    
    func getCalismalar()
    {
        Hud.sharedHud.show(self.navigationController!)
        // User's location
        let userGeoPoint = PFUser.currentUser()!["location"] as! PFGeoPoint

        let query = PFQuery(className:"Calisma")
        
        query.whereKey("location", nearGeoPoint: userGeoPoint, withinKilometers: 150)
        query.whereKey("endDate", greaterThan: NSDate())
        query.includeKey("users")
        query.findObjectsInBackgroundWithBlock {
            (objects: [PFObject]?, error: NSError?) -> Void in
            Hud.sharedHud.hide(self.navigationController!)
            self.refreshControl.endRefreshing()
            if error == nil {
                // The find succeeded.
                print("Successfully retrieved \(objects!.count) scores.")
                // Do something with the found objects
                if let objects = objects as [PFObject]! {
                        self.tableViewDataSource = objects
                        self.tableView.reloadData()
                }
            } else {
                // Log details of the failure
                print("Error: \(error!) \(error!.userInfo)")
            }
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        manager.delegate = nil
        manager.stopUpdatingLocation()
        print("locations = \(locations)")
        
        let user = PFUser.currentUser()!
        
        user["location"] = PFGeoPoint(location: locations[0])
        
        user.saveInBackgroundWithBlock { (success, error) -> Void in
            if success
            {
                self.getCalismalar()
            }
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return tableViewDataSource.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("CalismaCell", forIndexPath: indexPath)
        
        let calisma = tableViewDataSource[indexPath.row]
        
        // Configure the cell...
        cell.textLabel?.text = calisma["title"] as? String
        cell.detailTextLabel?.text = calisma["description"] as? String
        
        return cell
        
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 70
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        performSegueWithIdentifier("CalismaDetay", sender: indexPath)
    }

    func searchBarSearchButtonClicked(searchBar: UISearchBar)
    {
        if searchBar.text != ""
        {
            Hud.sharedHud.show(self.navigationController!)
            // User's location
            let userGeoPoint = PFUser.currentUser()!["location"] as! PFGeoPoint
            
            let query = PFQuery(className:"Calisma")
            
            query.whereKey("title", containsString: searchBar.text)
            query.whereKey("location", nearGeoPoint: userGeoPoint, withinKilometers: 150)
            query.whereKey("endDate", greaterThan: NSDate())
            query.includeKey("users")
            query.findObjectsInBackgroundWithBlock {
                (objects: [PFObject]?, error: NSError?) -> Void in
                Hud.sharedHud.hide(self.navigationController!)
                self.refreshControl.endRefreshing()
                if error == nil {
                    // The find succeeded.
                    print("Successfully retrieved \(objects!.count) scores.")
                    // Do something with the found objects
                    if let objects = objects as [PFObject]! {
                        self.tableViewDataSource = objects
                        self.tableView.reloadData()
                    }
                } else {
                    // Log details of the failure
                    print("Error: \(error!) \(error!.userInfo)")
                }
            }

        }
    }
    
    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if segue.identifier == "CalismaDetay"
        {
            let indexPath = sender as! NSIndexPath
            let calisma = tableViewDataSource[indexPath.row]
            
            let vc : CalismaDetayViewController = segue.destinationViewController as! CalismaDetayViewController
            vc.calisma = calisma
        }
    }

}
