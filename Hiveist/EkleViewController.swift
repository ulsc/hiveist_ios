//
//  EkleViewController.swift
//  Hiveist
//
//  Created by Ulas Can Cengiz on 30/09/15.
//  Copyright © 2015 iOSBootcamp. All rights reserved.
//

import UIKit
import MapKit
import Parse

class EkleViewController: UIViewController, UITextFieldDelegate, SelectDateDelegate {

    @IBOutlet var map: MKMapView!
    @IBOutlet var txtTitle: UITextField!
    @IBOutlet var txtDescription: UITextField!
    @IBOutlet var txtStartDate: UITextField!
    @IBOutlet var txtEndDate: UITextField!
    @IBOutlet var btnAdd: UIButton!
    
    @IBOutlet var recognizer: UILongPressGestureRecognizer!
    
    var startDate : NSDate?
    var endDate : NSDate?
    
    var isSelectingDateStartingDate = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addTapped(sender: UIButton)
    {
        let calisma = PFObject(className:"Calisma")
        
        calisma["creator"] = PFUser.currentUser()
        calisma["title"] = txtTitle.text ?? "Başlık Girilmemiş"
        calisma["description"] = txtDescription.text ?? "Açıklama Girilmemiş"
        calisma["users"] = [PFUser.currentUser()!]
        
        calisma["startDate"] = startDate ?? NSDate()
        calisma["endDate"] = endDate ?? NSDate()
        
        if self.map.annotations.count > 0
        {
            calisma["location"] = PFGeoPoint(latitude: self.map.annotations[0].coordinate.latitude, longitude: self.map.annotations[0].coordinate.longitude)
        }
        else
        {
            calisma["location"] = PFGeoPoint(latitude: 41, longitude: 30)
        }
        
        calisma.saveInBackgroundWithBlock {
            (success: Bool, error: NSError?) -> Void in
            if (success) {
                // The object has been saved.
                SUCCESS_ADD.show()
                
                self.txtTitle.text = ""
                self.txtDescription.text = ""
                self.txtStartDate.text = ""
                self.txtEndDate.text = ""
                self.map.removeAnnotations(self.map.annotations)
            } else {
                // There was a problem, check error.description
                ERROR_WRONG_CREDENTIALS.show()
            }
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }

    @IBAction func mapPressed(sender: UILongPressGestureRecognizer)
    {
        let point : CGPoint = sender.locationInView(self.map)
        let location : CLLocationCoordinate2D = self.map.convertPoint(point, toCoordinateFromView: self.map)
        
        let annotation : MKPointAnnotation = MKPointAnnotation()
        annotation.coordinate = location
        
        map.removeAnnotations(map.annotations)
        map.addAnnotation(annotation)
        
        map.selectAnnotation(annotation, animated: true)
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool
    {
        if textField.tag == 1
        {
            isSelectingDateStartingDate = true
            performSegueWithIdentifier("SelectDate", sender: textField)
        }
        else if textField.tag == 2
        {
            if startDate != nil
            {
                isSelectingDateStartingDate = false
                performSegueWithIdentifier("SelectDate", sender: textField)
            }
        }
        
        
        return false
    }
    
    func dateSelected(date: NSDate)
    {
        if isSelectingDateStartingDate
        {
            txtStartDate.text = Utils.addStringFromDate(date)
            startDate = date
        }
        else
        {
            txtEndDate.text = Utils.addStringFromDate(date)
            endDate = date
        }
    }
    
    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if segue.identifier == "SelectDate"
        {
            let vc : SelectDateViewController = segue.destinationViewController as! SelectDateViewController
            vc.delegate = self
            
            if sender!.tag == 1
            {
                vc.startDate = NSDate()
            }
            else if sender!.tag == 2
            {
                vc.startDate = startDate
            }
        }
    }

}
