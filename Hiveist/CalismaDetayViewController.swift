//
//  CalismaDetayViewController.swift
//  Hiveist
//
//  Created by Ulas Can Cengiz on 30/09/15.
//  Copyright © 2015 iOSBootcamp. All rights reserved.
//

import UIKit
import MapKit
import Parse

class CalismaDetayViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var map: MKMapView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var tableView: UITableView!
    
    var calisma : PFObject!
    
    var tableViewDataSource : [PFUser] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableViewDataSource = calisma["users"] as! [PFUser]
        tableView.reloadData()
        
        setMapViewLocation()
        
        if !tableViewDataSource.contains(PFUser.currentUser()!)
        {
            let addItem = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: "addTapped")
            
            self.navigationItem.setRightBarButtonItem(addItem, animated: true)
        }
        else
        {
            let cancelItem = UIBarButtonItem(barButtonSystemItem: .Cancel, target: self, action: "cancelTapped")
            
            self.navigationItem.setRightBarButtonItem(cancelItem, animated: true)
        }
        
        lblTitle.text = calisma["title"] as? String
        
        lblDate.text = Utils.hiveStringFromDates(calisma["startDate"] as! NSDate, endDate: calisma["endDate"] as! NSDate)
        
        lblDescription.text = calisma["description"] as? String
        // Do any additional setup after loading the view.
    }
    
    func setMapViewLocation()
    {
        let geoPoint : PFGeoPoint = calisma["location"] as! PFGeoPoint
        let location : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: geoPoint.latitude, longitude: geoPoint.longitude)
        let span : MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
        let region : MKCoordinateRegion = MKCoordinateRegion(center: location, span: span)
        
        map.setRegion(region, animated: true)
        
        let annotation : MKPointAnnotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = calisma["title"] as? String
        annotation.subtitle = calisma["description"] as? String
        
        map.removeAnnotations(map.annotations)
        map.addAnnotation(annotation)
        
        map.selectAnnotation(annotation, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addTapped()
    {
        print("Add Tapped")
        
        calisma.addObjectsFromArray([PFUser.currentUser()!], forKey: "users")
        calisma.saveInBackgroundWithBlock { (success, error) -> Void in
            if success
            {
                self.navigationItem.setRightBarButtonItem(nil, animated: true)
                self.tableViewDataSource = self.calisma["users"] as! [PFUser]
                let cancelItem = UIBarButtonItem(barButtonSystemItem: .Cancel, target: self, action: "cancelTapped")
                self.navigationItem.setRightBarButtonItem(cancelItem, animated: true)
                self.tableView.reloadData()
            }
        }
    }
    
    func cancelTapped()
    {
        print("Cancel Tapped")
        
        if (calisma["users"] as! [PFUser]).count > 1
        {
            calisma.removeObjectsInArray([PFUser.currentUser()!], forKey: "users")
            calisma.saveInBackgroundWithBlock { (success, error) -> Void in
                if success
                {
                    self.navigationItem.setRightBarButtonItem(nil, animated: true)
                    self.tableViewDataSource = self.calisma["users"] as! [PFUser]
                    let addItem = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: "addTapped")
                    self.navigationItem.setRightBarButtonItem(addItem, animated: true)
                    self.tableView.reloadData()
                }
            }
        }
        else
        {
            calisma.deleteInBackgroundWithBlock({ (success, error) -> Void in
                if success
                {
                    self.navigationController?.popViewControllerAnimated(true)
                }
            })
        }
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return tableViewDataSource.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("KatilimciCell", forIndexPath: indexPath)
        
        let katilimci = tableViewDataSource[indexPath.row]
        
        // Configure the cell...
        cell.textLabel?.text = katilimci["name"] as? String
        cell.detailTextLabel?.text = katilimci["occupation"] as? String
        
        return cell
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
