//
//  AyarlarViewController.swift
//  Hiveist
//
//  Created by Ulas Can Cengiz on 30/09/15.
//  Copyright © 2015 iOSBootcamp. All rights reserved.
//

import UIKit
import Parse
import FDTake

class AyarlarViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, FDTakeDelegate {

    @IBOutlet var btnProfileImage: UIButton!
    @IBOutlet var txtName: UITextField!
    @IBOutlet var txtOccupation: UITextField!
    @IBOutlet var btnSave: UIButton!
    
    var takeController : FDTakeController = FDTakeController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let user = PFUser.currentUser()!
        
        txtName.text = user["name"] as? String
        txtOccupation.text = user["occupation"] as? String
        
        let userImageFile = user["image"] as! PFFile
        userImageFile.getDataInBackgroundWithBlock {
            (imageData: NSData?, error: NSError?) -> Void in
            if error == nil {
                if let imageData = imageData {
                    let image = UIImage(data:imageData)
                    self.btnProfileImage.setBackgroundImage(image, forState: .Normal)
                }
            }
        }
        
        takeController.delegate = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveTapped(sender: UIButton)
    {
        let imageData = UIImageJPEGRepresentation(btnProfileImage.backgroundImageForState(.Normal)!, 0.7)
        let imageFile = PFFile(name:"image.png", data:imageData!)
        
        let user = PFUser.currentUser()!
        user["name"] = txtName.text
        user["occupation"] = txtOccupation.text
        user["image"] = imageFile
        user.saveInBackgroundWithBlock({ (success, error) -> Void in
            if success
            {
                self.navigationController?.popViewControllerAnimated(true)
            }
            else
            {
                ERROR_WRONG_CREDENTIALS.show()
            }
        })
    }

    @IBAction func photoTapped(sender: AnyObject)
    {
        takeController.takePhotoOrChooseFromLibrary()
    }
    
    func takeController(controller: FDTakeController!, gotPhoto photo: UIImage!, withInfo info: [NSObject : AnyObject]!) {
        self.btnProfileImage.setBackgroundImage(photo, forState: .Normal)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
