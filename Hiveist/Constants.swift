//
//  Constants.swift
//  Hiveist
//
//  Created by Ulas Can Cengiz on 01/10/15.
//  Copyright © 2015 iOSBootcamp. All rights reserved.
//

import Foundation
import UIKit


let PARSE_APP_ID = "lMBNKjjiajDsbKlSHT95fhZHE5EUHFIGqW2HcWty"
let PARSE_CLIENT_KEY = "6FwmQVGk525ZHxJo2wmyxcMQxUPUmHGSu5mDtcKr"

// MARK: Segue Identifiers

let SEGUE_LOGGED_IN = "LoggedIn"
let SEGUE_REGISTER = "Register"

// MARK: Errors

let ERROR_WRONG_CREDENTIALS = UIAlertView(title: "Error", message: "Wrong Credentials", delegate: nil, cancelButtonTitle: "Ok")
let SUCCESS_REGISTER = UIAlertView(title: "Success", message: "Register Successful", delegate: nil, cancelButtonTitle: "Ok")
let SUCCESS_ADD = UIAlertView(title: "Success", message: "Add Successful", delegate: nil, cancelButtonTitle: "Ok")