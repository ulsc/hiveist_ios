//
//  SelectDateViewController.swift
//  Hiveist
//
//  Created by Ulas Can Cengiz on 02/10/15.
//  Copyright © 2015 iOSBootcamp. All rights reserved.
//

import UIKit

protocol SelectDateDelegate
{
    func dateSelected(date: NSDate)
}

class SelectDateViewController: UIViewController {
    
    var delegate : SelectDateDelegate?

    @IBOutlet var picker: UIDatePicker!
    @IBOutlet var btnSelect: UIButton!
    @IBOutlet var btnCancel: UIButton!
    
    var startDate : NSDate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        picker.minimumDate = startDate
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func selectTapped(sender: AnyObject)
    {
        self.delegate?.dateSelected(picker.date)
        dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func cancelTapped(sender: AnyObject)
    {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
