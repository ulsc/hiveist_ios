//
//  RegisterViewController.swift
//  Hiveist
//
//  Created by Ulas Can Cengiz on 30/09/15.
//  Copyright © 2015 iOSBootcamp. All rights reserved.
//

import UIKit
import Parse

class RegisterViewController: UIViewController {
    
    
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtPass: UITextField!
    @IBOutlet var txtRePass: UITextField!
    @IBOutlet var btnRegister: UIButton!
    
    var email : String?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtEmail.text = email
    }
    
    @IBAction func registerTapped(sender: UIButton)
    {
        // Register butonuna basıldı
        if txtEmail.text == "" || txtPass.text == "" || txtRePass.text == ""
        {
            // Burada bir sorun var
            ERROR_WRONG_CREDENTIALS.show()
        }
        else if txtPass.text != txtRePass.text
        {
            ERROR_WRONG_CREDENTIALS.show()
        }
        else
        {
            let user = PFUser()
            
            user.username = txtEmail.text
            user.password = txtPass.text
            user.email = txtEmail.text
            user["location"] = PFGeoPoint(latitude: 41, longitude: 29)
            
            user.signUpInBackgroundWithBlock {
                (succeeded: Bool, error: NSError?) -> Void in
                if error != nil {
                    ERROR_WRONG_CREDENTIALS.show()
                } else {
                    // Hooray! Let them use the app now.
                    SUCCESS_REGISTER.show()
                    self.dismissViewControllerAnimated(true, completion: nil)
                }
            }
        }
        
    }

    @IBAction func loginTapped(sender: UIButton)
    {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
