//
//  ProfilViewController.swift
//  Hiveist
//
//  Created by Ulas Can Cengiz on 30/09/15.
//  Copyright © 2015 iOSBootcamp. All rights reserved.
//

import UIKit
import Parse

class ProfilViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblOccupation: UILabel!
    @IBOutlet var tableView: UITableView!
    
    var tableViewDataSource : [PFObject] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getCalismalar()
        
        let addItem = UIBarButtonItem(barButtonSystemItem: .Edit, target: self, action: "editTapped")
        
        self.navigationItem.setRightBarButtonItem(addItem, animated: true)
    }
    
    override func viewWillAppear(animated: Bool)
    {
        let user = PFUser.currentUser()!
        
        lblName.text = user["name"] as? String
        lblOccupation.text = user["occupation"] as? String
        
        let userImageFile = user["image"] as! PFFile
        userImageFile.getDataInBackgroundWithBlock {
            (imageData: NSData?, error: NSError?) -> Void in
            if error == nil {
                if let imageData = imageData {
                    let image = UIImage(data:imageData)
                    self.imgProfile.image = image
                }
            }
        }
        
    }
    
    func editTapped()
    {
        performSegueWithIdentifier("Ayarlar", sender: self)
    }
    
    func getCalismalar()
    {
        let query = PFQuery(className:"Calisma")
        
        query.whereKey("creator", equalTo: PFUser.currentUser()!)
        query.findObjectsInBackgroundWithBlock {
            (objects: [PFObject]?, error: NSError?) -> Void in
            if error == nil {
                // The find succeeded.
                print("Successfully retrieved \(objects!.count) scores.")
                // Do something with the found objects
                if let objects = objects as [PFObject]! {
                    self.tableViewDataSource = objects
                    self.tableView.reloadData()
                }
            } else {
                // Log details of the failure
                print("Error: \(error!) \(error!.userInfo)")
            }
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return tableViewDataSource.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("CalismaCell", forIndexPath: indexPath)
        
        let calisma = tableViewDataSource[indexPath.row]
        
        // Configure the cell...
        cell.textLabel?.text = calisma["title"] as? String
        cell.detailTextLabel?.text = calisma["description"] as? String
        
        return cell
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 70
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        performSegueWithIdentifier("ProfilToCalismaDetay", sender: indexPath)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if segue.identifier == "ProfilToCalismaDetay"
        {
            let indexPath = sender as! NSIndexPath
            let calisma = tableViewDataSource[indexPath.row]
            
            let vc : CalismaDetayViewController = segue.destinationViewController as! CalismaDetayViewController
            vc.calisma = calisma
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
