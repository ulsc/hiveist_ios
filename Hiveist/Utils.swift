//
//  Utils.swift
//  Hiveist
//
//  Created by Ulas Can Cengiz on 02/10/15.
//  Copyright © 2015 iOSBootcamp. All rights reserved.
//

import Foundation
import JGProgressHUD

class Utils
{
    class func hiveStringFromDates(startDate: NSDate, endDate: NSDate) -> String
    {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "d MMMM - HH:mm"
        
        let startString = dateFormatter.stringFromDate(startDate)
        let endString = dateFormatter.stringFromDate(endDate)
        
        let finalDateString = "\(startString) => \(endString)"
        
        return finalDateString
    }
    
    class func addStringFromDate(date: NSDate) -> String
    {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "d MMMM - HH:mm"
        
        let dateString = dateFormatter.stringFromDate(date)
        
        return dateString
    }

}

// Progress HUD Singleton
class Hud
{
    
    static let sharedHud = Hud()
    
    var h : JGProgressHUD?
    
    func show(controller: UIViewController)
    {
        h = JGProgressHUD(style: JGProgressHUDStyle.ExtraLight)
        h!.textLabel.text = "Yükleniyor..."
        h!.showInView(controller.view)
        controller.view.userInteractionEnabled = false
    }
    
    func hide(controller: UIViewController)
    {
        if let hud = h
        {
            hud.dismiss()
        }
        controller.view.userInteractionEnabled = true
    }
    
}
