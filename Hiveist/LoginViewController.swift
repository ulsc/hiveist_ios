//
//  LoginViewController.swift
//  Hiveist
//
//  Created by Ulas Can Cengiz on 30/09/15.
//  Copyright © 2015 iOSBootcamp. All rights reserved.
//

import UIKit
import Parse
import ParseFacebookUtilsV4

class LoginViewController: UIViewController {

    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtPass: UITextField!
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet var btnFacebook: UIButton!
    @IBOutlet var btnRegister: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(animated: Bool)
    {
        let currentUser = PFUser.currentUser()
        if currentUser != nil {
            // Do stuff with the user
            self.performSegueWithIdentifier(SEGUE_LOGGED_IN, sender: self)
        }
    }
    
    @IBAction func loginTapped(sender: UIButton)
    {
        // Kullanıcı Login Butonuna Bastı
        print("Login Tapped")
        
        PFUser.logInWithUsernameInBackground(txtEmail.text!, password:txtPass.text!) {
            (user: PFUser?, error: NSError?) -> Void in
            if user != nil {
                // Do stuff after successful login.
                self.performSegueWithIdentifier(SEGUE_LOGGED_IN, sender: self)
            } else {
                // The login failed. Check error to see why.
                
                ERROR_WRONG_CREDENTIALS.show()
            }
        }
        
    }
    
    @IBAction func facebookTapped(sender: UIButton)
    {
        // Kullanıcı Facebook Butonuna Bastı
        print("Facebook Tapped")
        
        PFFacebookUtils.logInInBackgroundWithReadPermissions(["email"]) {
            (user: PFUser?, error: NSError?) -> Void in
            if let user = user {
                if user.isNew {
                    print("User signed up and logged in through Facebook!")
                    user["location"] = PFGeoPoint(latitude: 41, longitude: 29)
                    
                    user.signUpInBackgroundWithBlock {
                        (succeeded: Bool, error: NSError?) -> Void in
                        if error != nil {
                            ERROR_WRONG_CREDENTIALS.show()
                        } else {
                            // Hooray! Let them use the app now.
                            self.performSegueWithIdentifier(SEGUE_LOGGED_IN, sender: self)
                        }
                    }
                } else {
                    print("User logged in through Facebook!")
                    self.performSegueWithIdentifier(SEGUE_LOGGED_IN, sender: self)
                }
            } else {
                print("Uh oh. The user cancelled the Facebook login.")
                ERROR_WRONG_CREDENTIALS.show()
            }
        }
    }

    @IBAction func registerTapped(sender: UIButton)
    {
        // Kullanıcı Register Butonuna Bastı
        print("Register Tapped")
        
        performSegueWithIdentifier(SEGUE_REGISTER, sender: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    

    
    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if segue.identifier == SEGUE_REGISTER
        {
            let vc : RegisterViewController = segue.destinationViewController as! RegisterViewController
            vc.email = txtEmail.text
        }
    }
    

}
